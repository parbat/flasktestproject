
from app import flasktestdb
from app.models import User, Post


def register_user(user):
    users = flasktestdb.db.users

    result = users.insert_one(user.get_as_json())
    return result


def is_user_registered(email):
    users = flasktestdb.db.users
    # print users.find({"email":email}).count()
    return users.find({"email": email}).count()


def is_password_correct(email, password):
    users = flasktestdb.db.users
    return users.find({"email": email, "password": password}).count() > 0


def get_all_users():
    users = flasktestdb.db.users
    user_list = users.find()
    return user_list


def get_user_by_email(email):
    users = flasktestdb.db.users
    return User.build_from_json(users.find_one({"email": email}))


def get_user_by_id(id):
    users = flasktestdb.db.users
    return User.build_from_json(users.find_one({'_id': id}))


def edit_user(user_old, user_new):
    users = flasktestdb.db.users

    result = users.update_one(
        {"email": user_old.email},
        {
            "$set": {"username": user_new.username,
                     "about_me": user_new.about_me}
        }
    )
    return result


def add_post(post):
    posts = flasktestdb.db.posts

    result = posts.insert_one(post.get_as_json())
    return result


def get_post_by_id(id):
    posts = flasktestdb.db.posts
    return Post.build_from_json(posts.find_one({'_id': id}))


def get_posts(email):
    posts = flasktestdb.db.posts
    user = get_user_by_email(email)

    posts_cursor = posts.find({"user_id": user.get_id()})
    post_dict = []
    for post in posts_cursor:
        post_dict.append({'author': user, 'body': post.get('body', None), '_id': post.get('_id')})
    # To get the latest post first
    return post_dict[::-1]


def get_all_posts():
    users_coll = flasktestdb.db.users
    posts_dict = []
    for user in users_coll.find():
        posts_dict += get_posts(user['email'])

    print get_sorted_posts()
    return sorted(posts_dict)


def get_sorted_posts():
    posts = flasktestdb.db.posts
    posts_dict = []
    for post in posts.find().sort([("_id", -1)]):
        user = get_user_by_id(post['user_id'])
        postobj = get_post_by_id(post['_id'])
        posts_dict.append({'author': user, 'body': post.get('body', None), '_id': post.get('_id'),
                           'time': postobj.time})
        print postobj.time
    return posts_dict
