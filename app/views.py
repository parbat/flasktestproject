from flask import session
from flask import url_for
from flask_login import login_required
from app import lm

from app import testapp
from flask import render_template, jsonify, flash, redirect

from app.models import User, Post
from .forms import LoginForm, RegistrationForm, EditForm, PostForm, ForgotPassForm
from app import dboperations

from app import mail
from flask_mail import Message


@testapp.route('/login', methods=['GET', 'POST'])
def login():
    login_form = LoginForm()
    if login_form.validate_on_submit():
        user = User(email=login_form.email.data, password=login_form.password.data)
        flash('Login successful for email= %s' % user.email)
        session['logged_in'] = True
        session['email'] = user.email
        return redirect('/index')
    if len(login_form.password.errors) > 0:
        return render_template('login.html',
                               title='Sign In',
                               form=login_form,
                               forgotpass=True)
    return render_template('login.html',
                           title='Sign In',
                           form=login_form)


@testapp.route('/register', methods=['GET', 'POST'])
def register():
    # TODO Send registration email to verify email
    register_form = RegistrationForm()
    if register_form.validate_on_submit():
        # register the user to the system database
        new_user = User(email=register_form.email.data,
                        password=register_form.password.data,
                        username=register_form.username.data,
                        about_me=register_form.about_me.data)
        dboperations.register_user(new_user)
        # show success message
        flash('Account Successfully registered')
        return redirect('/index')
    return render_template('register.html',
                           title='Register',
                           form=register_form)


@testapp.route('/')
@testapp.route('/index')
# @login_required
def index():
    if not session.get('logged_in'):
        return redirect(url_for('login'))

    logged_user = dboperations.get_user_by_email(session['email'])
    # posts = dboperations.get_all_posts()
    posts = dboperations.get_sorted_posts()
    return render_template('index.html',
                           title='Home',
                           user=logged_user,
                           posts=posts)


@testapp.route('/logout')
def logout():
    session['logged_in'] = False
    return redirect('/login')


@testapp.route('/user/<email>')
def user(email):
    if not session.get('logged_in'):
        return redirect(url_for('login'))

    viewed_user = dboperations.get_user_by_email(email)
    if viewed_user is None:
        flash('Email %s not found.' % email)
        return redirect(url_for('index'))
    posts = dboperations.get_posts(email)
    return render_template('user.html',
                           user=viewed_user,
                           posts=posts)


@testapp.route('/edit', methods=['GET', 'POST'])
def edit():
    if not session.get('logged_in'):
        return redirect(url_for('login'))

    user_old = dboperations.get_user_by_email(session['email'])
    edit_form = EditForm()
    if edit_form.validate_on_submit():
        user_new = User(username=edit_form.username.data,
                        about_me=edit_form.about_me.data)
        print ('Username: ', user_new.username, "\nAbout Me: ", user_new.about_me)
        dboperations.edit_user(user_old, user_new)
        flash('Your changes have been saved.')
        return redirect(url_for('edit'))
    else:
        edit_form.username.data = user_old.username
        edit_form.about_me.data = user_old.about_me

    return render_template('edit.html',
                           title='Edit',
                           form=edit_form)


@testapp.route('/new_post', methods=['GET', 'POST'])
def new_post():
    if not session.get('logged_in'):
        return redirect(url_for('login'))

    logged_user = dboperations.get_user_by_email(session['email'])
    post_form = PostForm()
    if post_form.validate_on_submit():
        post = Post(body=post_form.post_body.data,
                    user_id=logged_user.get_id())
        dboperations.add_post(post)
        flash('Your post has been updated')
        return redirect(url_for('index'))
    return render_template('new_post.html',
                           title='New Post',
                           form=post_form)


@testapp.route('/forgotpass', methods=['GET', 'POST'])
def forgot_password():
    forgot_pass_form = ForgotPassForm()
    if forgot_pass_form.validate_on_submit():
        email = forgot_pass_form.email.data
        password = dboperations.get_user_by_email(email).password
        msg = Message("Password: " + password,
                      sender="flasktestapp.parbat@gmail.com",
                      recipients=[email])
        mail.send(msg)
        flash('Password has been sent to your mail')
        return redirect(url_for('index'))

    return render_template('forgotpass.html',
                           title='Forgot Password',
                           form=forgot_pass_form)


@lm.user_loader
def user_loader(user_id):
    """Given *user_id*, return the associated User object.

    :param unicode user_id: user_id (email) user to retrieve
    """
    return User("parbat.1993@gmail.com", "1234")
