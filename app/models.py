from bson.objectid import ObjectId


# TODO Add user followers
class User:

    def __init__(self, user_id=None, email=None, password=None, username=None, about_me=None):
        if user_id is None:
            self._id = ObjectId()
        else:
            self._id = user_id
        self.email = email
        self.password = password
        self.username = username
        self.about_me = about_me

    def get_as_json(self):
        return self.__dict__

    @staticmethod
    def build_from_json(json_data):
        """ Method used to build User objects from JSON data returned from MongoDB """
        if json_data is not None:
            try:
                return User(
                    json_data.get('_id', None),
                    json_data['email'],
                    json_data['password'],
                    json_data.get('username', None),
                    json_data.get('about_me', None)
                )
            except KeyError as e:
                raise Exception("Key not found in json_data: {}".format(e.message))
        else:
            return None
            # raise Exception("No data to create User from!")

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return self._id

    def __repr__(self):
        return '<User %r>' % self.username


class Post:

    def __init__(self, post_id=None, body=None, user_id=None):
        if post_id is None:
            self._id = ObjectId()
        else:
            self._id = post_id
        self.body = body
        self.user_id = user_id
        self.time = self._id.generation_time

    def get_as_json(self):
        return self.__dict__

    @staticmethod
    def build_from_json(json_data):
        if json_data is not None:
            try:
                return Post(
                    json_data.get('_id', None),
                    json_data['body'],
                    json_data['user_id']
                )
            except KeyError as e:
                raise Exception("Key not found in json_data: {}".format(e.message))
        else:
            return None
            # raise Exception("No data to create User from!")

    def __repr__(self):
        return '<Post %r>' % self.body
