from flask import Flask
from flask_login import LoginManager
from flask_pymongo import PyMongo
from flask_mail import Mail

testapp = Flask(__name__)
testapp.config.from_object('config')

testapp.config['MONGO_DBNAME'] = 'flasktestdb'
testapp.config['MONGO_URI'] = 'mongodb://localhost:27017/flasktestdb'
flasktestdb = PyMongo(testapp)

testapp.config['MAIL_SERVER']='smtp.gmail.com'
testapp.config['MAIL_PORT'] = 465
testapp.config['MAIL_USERNAME'] = 'flasktestapp.parbat@gmail.com'
testapp.config['MAIL_PASSWORD'] = 'flasktestapp'
testapp.config['MAIL_USE_TLS'] = False
testapp.config['MAIL_USE_SSL'] = True
mail = Mail(testapp)

lm = LoginManager()
lm.init_app(testapp)
lm.login_view = 'login'
from app import views