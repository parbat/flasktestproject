from flask_wtf import Form
from wtforms import StringField, BooleanField, PasswordField, TextAreaField
from wtforms import validators
from wtforms.validators import Length

from app import dboperations


class LoginForm(Form):
    email = StringField('email', [
        validators.DataRequired(),
        validators.Email(message='That\'s not a valid email address.')
    ])
    password = PasswordField('password', [
        validators.DataRequired()
    ])
    remember_me = BooleanField('remember_me', default=False)

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        validated = Form.validate(self)
        if not validated:
            return False

        if not dboperations.is_user_registered(self.email.data):
            self.email.errors.append('Email not registered yet')
            return False

        if not dboperations.is_password_correct(self.email.data, self.password.data):
            self.password.errors.append('Invalid password')
            return False

        return True


# What sorcery is this??
class RegistrationForm(Form):
    email = StringField('email', [
        validators.DataRequired(),
        validators.Email(message='That\'s not a valid email address.')
    ])
    password = PasswordField('email', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')
    username = StringField('username', [
        validators.DataRequired()
    ])
    about_me = TextAreaField('About Me', validators=[Length(min=0, max=140)])

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False

        if dboperations.is_user_registered(self.email.data):
            self.email.errors.append('Email Already registered')
            return False

        return True


class EditForm(Form):
    username = StringField('username', [
        validators.DataRequired()
    ])
    about_me = TextAreaField('About Me', validators=[Length(min=0, max=140)])


class PostForm(Form):
    post_body = TextAreaField('Post',  validators=[Length(min=1, max=140)])


class ForgotPassForm(Form):
    email = StringField('email', [
        validators.DataRequired(),
        validators.Email(message='That\'s not a valid email address.')
    ])

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False

        if not dboperations.is_user_registered(self.email.data):
            self.email.errors.append('Email not registered yet')
            return False

        # Cost another 2 hours
        return True

